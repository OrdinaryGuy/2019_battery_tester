// Napätie lin. regulátora 4,94 V

// Kinžnice pre microSD
#include <SPI.h>
#include <SD.h>

// Knižnice pre AD prevodník
#include <Wire.h>
#include <Adafruit_ADS1015.h>

// typ AD prevodníka
// Adafruit_ADS1115 ads;  /* 16-bitová verzia */
Adafruit_ADS1015 ads;     /* 12-bitová verzia */

// CS pin pre SD
#define chipSelect 10 
// analog. vstupy
#define voltage1   A0
#define voltage2   A1
#define voltage3   A2
// relé
#define relay       4
// spustenie programu
#define startProg   2
// aktivácia merania vnút. odporu
#define intResPin   7
// informačné LED
#define greenLED    8
#define redLED      9

// ------------------------ Nastaviteľné parametre --------------------------
const int tresholdVoltage         = 100; // Ak je rozdiel napätí menší než 100 mV, arduino chápe, že dva vstupy sú na spoločnej svorke
const int criticalVoltage         = 900; // kritické napätie v mV
const long int interval           = 5;   // Interval medzi meraniami v sekundách
const int recoveryTime            = 10;  // doba relaxácie batérie v sekundách, ak rovný nule vnútorný odpor sa nebude merať
const long int intervalResistance = 15;  // doba medzi meraniami v minútach
// --------------------------------------------------------------------------

// Napätie na analog vstupoch
unsigned long int voltageValue1;
unsigned long int voltageValue2;
unsigned long int voltageValue3;

// Napätie na bočníku
long int currentValue;

unsigned long delOfProgram;
unsigned int directoryNumber = 0;
char dir [8];
char pathToDischarge [16];
char pathToIntResist [16];
char SDdata [100];

long int dischargeMeasurement = 0; // počet meraní hl. cyklu
long int measurementResistance = 0; // počet meraní vnút. odporu

bool progRun = false;
bool intResActive;

// SD objekt
File dataFile;

void setup() {
  Serial.begin(9600);
  
  pinMode(greenLED, OUTPUT);
  pinMode(redLED, OUTPUT);
  pinMode(relay, OUTPUT);
  pinMode(startProg, INPUT);
  pinMode(intResPin, INPUT);
  
  // Inicializácia AD prevodníka
  
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  ads.begin();
  setupInitialize();
  attachInterrupt(digitalPinToInterrupt(startProg), progStart, RISING);

  progReady();
  progInitialize();
}

void loop() {
  // záleží na poradí funkcii
  dischargeProgram();
  internalResistance();
}

void progInitialize () {
  // nemenné svetlo signalizuje bežiaci program
  digitalWrite(greenLED, HIGH);
  
  if (intResActive == false) {
    // čakacia doba pre zotavenie batérie
    delay (10000);
    dischargeProgram();
    digitalWrite(relay, HIGH);
  }
  
  delOfProgram = millis();
}

// kontrola všetkých potrebných informácii
void setupInitialize () {
  // stabilizácia analog. vstupov
  for (int j = 0; j < 3000; j++){
    voltageValue1 = analogRead(voltage1);
    voltageValue2 = analogRead(voltage2);
    voltageValue3 = analogRead(voltage3);
  }
  getVoltage();
  checkVoltage();

  // inicializácia SD karty
  Serial.print(F("Inicializácia SD karty..."));

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println(F(" problém s SD kartou."));
    errorMsg();
  }
  Serial.println(F(" karta inicializovaná."));

  do {
    directoryNumber++;
    sprintf(dir, "DIR_%d", directoryNumber); 
  } while(SD.exists(dir));
  SD.mkdir(dir);
  sprintf(pathToDischarge, "/%s/dsc.txt", dir);
  dataFile = SD.open(pathToDischarge, FILE_WRITE);
  dataFile.print(F("Kriticka hodnota napatia (mV): "));
  dataFile.println(criticalVoltage);
  dataFile.println(F("Prvé meranie je bez zataze"));
  dataFile.println(F("Meranie\tCas(s)\tU1(mV)\tU2(mV)\tU3(mV)\tI(mA)"));
  dataFile.close();
  sprintf(pathToIntResist, "/%s/res.txt", dir);
  dataFile = SD.open(pathToIntResist, FILE_WRITE);
  if (digitalRead(intResPin) == LOW) {
    dataFile.print(F("Funkcia deaktivovana"));
    intResActive = false;
  }
  else {
    dataFile.println(F("Meranie\tCas(s)\tdU1(mV)\tdU2(mV)\tdU3(mV)"));
    intResActive = true;
  }
  dataFile.close();
  Serial.println("\n ----------------------- \n Čaká sa na spustenie... \n -----------------------");

}

// ISR funkcia, ktorá rozbehne program
void progStart () {
  // opätovná stabilizácia analog. vstupov
  for (int j = 0; j < 3000; j++){
    voltageValue1 = analogRead(voltage1);
    voltageValue2 = analogRead(voltage2);
    voltageValue3 = analogRead(voltage3);
  }
  
  // deaktivácia interruptu
  detachInterrupt(digitalPinToInterrupt(startProg));
  // spustenie programu, ukončenie funkcie progReady
  progRun = true;
  Serial.println(F("\n Meranie spustené \n"));
}

// funkcia akumuláciou 128 meraní získa priemerné napätie na analog. pinoch
void getVoltage() {
  voltageValue1 = 0;
  voltageValue2 = 0;
  voltageValue3 = 0;
  for (int j = 0; j < 128; j++){
    voltageValue1 += analogRead(voltage1);
    voltageValue2 += analogRead(voltage2);
    voltageValue3 += analogRead(voltage3);
  }

  // Hodnota je násobená referenčným napätím v mV + kalibrácia
  voltageValue1 *= 4952;
  voltageValue2 *= 4952;
  voltageValue3 *= 4952;

  // Hodnota je delená počtom kumulácii*bitové rozlíšenie (128 * 1023)
  // Výsledná hodnota je namerená hodnota v mV
  voltageValue1 /= 130944;
  voltageValue2 /= 130944;
  voltageValue3 /= 130944;
}

void getCurrent() {
  currentValue = 0;
  for (int i = 0; i < 8; i++) {
  currentValue += ads.readADC_Differential_0_1(); 
  delay(1);
  }
  currentValue *= 1000; // Prúd v mA

  currentValue /= 24; // Bočník 200A = 75mV... 1A = 3 bity; teda delíme (bit/A) * počet kumulácii... 3 * 8
  // currentValue /= 96; // Bočník 50A = 75mV... 1A = 12 bitov
  // Zisk 8
  //currentValue = ads.readADC_Differential_0_1()*2/3;
}

// funkcia kontroluje napätie na svorkách a vypne program ak niektorá z hodnôt klesne pod kritickú hodnotu
void checkVoltage() {
  int minValue = 0;
  int maxValue = 0;
  int midValue = 0;

  // Určenie min hodnoty
  minValue = min(voltageValue1, voltageValue2);
  minValue = min(minValue, voltageValue3);

  // Určenie max hodnoty
  maxValue = max(voltageValue1, voltageValue2);
  maxValue = max(maxValue, voltageValue3);

  // Určenie prostrednej hodnoty
  if (voltageValue1 != minValue && voltageValue1 != maxValue) {midValue = voltageValue1;}
  else if (voltageValue2 != minValue && voltageValue2 != maxValue) {midValue = voltageValue2;}
  else {midValue = voltageValue3;}

  // Podmienka vybitej batérie
  if (minValue < criticalVoltage || 
  (midValue - minValue > tresholdVoltage && midValue - minValue < criticalVoltage) || 
  (maxValue - midValue > tresholdVoltage && maxValue - midValue < criticalVoltage)) {
    if (progRun == true) {
      digitalWrite(relay, LOW);
      Serial.println(F("\n Meranie ukončené"));
      progRun = false;
  
      // Signalizovanie ukončenia programu
      while (1) {
        digitalWrite(redLED, HIGH);
        digitalWrite(greenLED, LOW);
        delay(500);
        digitalWrite(redLED, LOW);
        digitalWrite(greenLED, HIGH);
        delay(500); 
      }
    }
    else {
      errorMsg();
    }
  }
}

// po úspešnej inicializácii funkcia oznamuje, že sa program môže začať
void progReady() {
  while (progRun == false) {
    digitalWrite(greenLED, HIGH);
    delay(500);
    digitalWrite(greenLED, LOW);
    delay(500);    
  }
}

void internalResistance () {
  // omeškanie spôsobené meraním vnút. odporu
  unsigned long intResDelay = 1000 * recoveryTime * measurementResistance;
  // +5 ms je na zaručenie, že najskôr prebehne dischargeProgram 
  unsigned long controlTime = 60000 * intervalResistance * measurementResistance + 5;
  if (intResActive == true && millis() - (delOfProgram + intResDelay) > controlTime) {
    measurementResistance++;
    
    // spustenie relaxácie batérie
    digitalWrite(relay, LOW);
    delay(1000 * recoveryTime - 1);

    // napätie po relaxácii
    getVoltage();
    long int voltageOff1 = voltageValue1;
    long int voltageOff2 = voltageValue2;
    long int voltageOff3 = voltageValue3;
    
    // spustenie záťaže
    digitalWrite(relay, HIGH);
    
    delay(2000);

    // napätie v záťaži
    getVoltage();

    // výpočet napäťového rozdielu
    long int voltageDiff1 = 0;
    long int voltageDiff2 = 0;
    long int voltageDiff3 = 0;
    voltageDiff1 = voltageOff1 - voltageValue1;
    voltageDiff2 = voltageOff2 - voltageValue2;
    voltageDiff3 = voltageOff3 - voltageValue3;
    
    sprintf(SDdata, "\n%ld \t %ld \t %ld \t %ld \t %ld \n",measurementResistance, 60 * intervalResistance * (measurementResistance - 1), voltageDiff1, voltageDiff2, voltageDiff3);
    dataFile = SD.open(pathToIntResist, FILE_WRITE);
        if (!SD.begin(chipSelect)) {
          digitalWrite(relay, LOW);
          Serial.println(" problém s SD kartou.");
          errorMsg();
        }
    dataFile.println(SDdata);
    dataFile.close();

    Serial.println(SDdata);

    checkVoltage();
  }
}

void dischargeProgram() {
  // omeškanie spôsobené meraním vnút. odporu
  unsigned long intResDelay = 1000 * recoveryTime * measurementResistance;
  if (millis() - (delOfProgram + intResDelay) > 1000 * interval * dischargeMeasurement) {
    dischargeMeasurement++;
    getVoltage();
    getCurrent();
    
    sprintf(SDdata, "%ld \t %ld \t %ld \t %ld \t %ld \t %ld", dischargeMeasurement, interval * (dischargeMeasurement - 1), voltageValue1, voltageValue2, voltageValue3, currentValue);
    dataFile = SD.open(pathToDischarge, FILE_WRITE);
        if (!SD.begin(chipSelect)) {
          digitalWrite(relay, LOW);
          Serial.println(F(" problém s SD kartou."));
          errorMsg();
        }
    dataFile.println(SDdata);
    dataFile.close();

    Serial.println(SDdata);
    checkVoltage();
    }
}

void errorMsg() {
  detachInterrupt(digitalPinToInterrupt(startProg));
  digitalWrite(greenLED, LOW);
  while (1) {
    digitalWrite(redLED, HIGH);
    delay(200);
    digitalWrite(redLED, LOW);
    delay(200); 
  }
}
